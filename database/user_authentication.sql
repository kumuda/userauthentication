-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 23, 2015 at 03:44 PM
-- Server version: 5.5.41-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `user_authentication`
--

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(300) NOT NULL,
  `password` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`) VALUES
(8, 'abcd', 'uddi4cAXGF4jA0jUl7NAbhe4jgYdr7pwJ5p6nNzo9ArdFJvCrzAmGsUJMjURd0Tx'),
(9, 'kumuda', 'fUMDye2yCqMnfqNv7Lqr7w7e5+I7C58wqVXc6Tnx+YJ4VPyPS+Jz3sdpj6mFyZ9y'),
(10, 'qwerty', 'ZOSs2MZoA04CFJgOv7oNr8+8meKzsVmZOckCxUMHIwQ920VfONwZemyCT/mctoXG'),
(11, 'qwe', 'GrZ4hbXtgKPs+FtfFOZN8K63ne9iD7JzMyFH2xMDVvWTwIfRi8qijZ/AjkLJ74Hy'),
(12, 'venkatesh', 'BkGvAvZ5eUfLYj6uXDNPJXGJ6CtiBHWTcj7K4ou5NZuES+V77mmHU0SnAdSsQBt4'),
(13, 'venky', '9VN1AtXCzCmioYSmIZ3nLYBwBQD4vlmM03qd/9WmdEadqZ3VyRvqJ3qQWv2U56vA');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
