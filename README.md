# README #

**Summary:**
* A RESTful api for user authentication and registration.
* 1.0


Set up:

1. Make sure you have installed maven.
2. Create a MySQL database by name "user_authentication". To import database: In the command line,
   change directory to database folder inside UserAuthentication/database
   and type: mysql -u root -p user_authentication < user_authentication.sql.   
3. In UserAuthentication/config.yml file, change the database username and password.
4. Build the project with maven.To deploy run the below command:
   java -jar target/UserAuthentication-0.0.1-SNAPSHOT.jar server config.yml 

  
Sample JsonObject to send valid request:

{"userName":"enter username",
"password":"enter password"
}