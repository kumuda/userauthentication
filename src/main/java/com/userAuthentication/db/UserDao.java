package com.userAuthentication.db;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import com.userAuthentication.mapper.UserMapper;
import com.userAuthentication.model.User;

@RegisterMapper(UserMapper.class)
public interface UserDao {

	@SqlUpdate("insert into user (username, password) values (:userName, :password)")
	public void insert(@Bind("userName") String userName,
			@Bind("password") String password);

	@SqlQuery("select username and password from user where id = :id")
	public User findUserById(@Bind("id") int id);

	@SqlQuery("select id, username, password from user where username = :userName")
	User findUserByUserName(@Bind("userName") String userName);

	@SqlUpdate("update user set password = :password, where username = :userName")
	public void update(@Bind("userName") String userName,
			@Bind("password") String password);

	@SqlUpdate("delete from user where username = :userName")
	public void delete(@BindBean User user);

}
