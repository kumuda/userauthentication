package com.userAuthentication.resources;

import io.dropwizard.hibernate.UnitOfWork;

import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.jasypt.util.password.StrongPasswordEncryptor;

import com.codahale.metrics.annotation.ExceptionMetered;
import com.codahale.metrics.annotation.Timed;
import com.userAuthentication.db.UserDao;
import com.userAuthentication.exception.ResponseException;
import com.userAuthentication.model.User;
//import com.userAuthentication.auth.Authorized;

@Path("/UserAuthentication")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class UserResource {

    private final UserDao userDao;
  
    public UserResource(UserDao userDao) {
        this.userDao = userDao;
    }

    @POST
    @Path("/register")
    @Timed
    @UnitOfWork
    @ExceptionMetered
    public User post(@Valid User user) {
    	System.out.println("Inside user method-------->");
        User existingUser = userDao.findUserByUserName(user.getUserName());
        if (existingUser != null) {
            ResponseException.formatAndThrow(Response.Status.NOT_ACCEPTABLE, "User already exists.");
            return existingUser;
        } else {
        	StrongPasswordEncryptor passwordEncryptor = new StrongPasswordEncryptor();
        	String encryptedPassword = passwordEncryptor.encryptPassword(user.getPassword());
              user.setPassword(encryptedPassword);
              userDao.insert(user.getUserName(), user.getPassword());
              return user;
        }

    }

    @POST
    @Path("/login")
    @Timed
    @UnitOfWork
    @ExceptionMetered
    public User put(@Valid User user) {
    	User existingUser = userDao.findUserByUserName(user.getUserName());
    	System.out.println(existingUser);
    	if(existingUser == null){
    		ResponseException.formatAndThrow(Response.Status.UNAUTHORIZED, "Invalid email / password");
    		return null;
    	}
    	else{
    		StrongPasswordEncryptor passwordEncryptor = new StrongPasswordEncryptor();
    		if(passwordEncryptor.checkPassword(user.getPassword(), existingUser.getPassword())){
    			return existingUser;
    		}
    		else{
    			ResponseException.formatAndThrow(Response.Status.UNAUTHORIZED, "Invalid email / password");
        		return null;
    		}
    	}
    }
   
}
