package com.userAuthentication;

import io.dropwizard.Configuration;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.server.SimpleServerFactory;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UserAuthenticationConfiguration extends Configuration{
	
   
    @NotNull
    private String userName;
   
    @NotNull
    private String password;
    
    @Valid
    @NotNull
    private DataSourceFactory database = new DataSourceFactory();
    
    public UserAuthenticationConfiguration() {
        setServerFactory(new SimpleServerFactory());
    }

    @JsonProperty
    public String getUserName() {
        return userName;
    }

    @JsonProperty
    public String getPassword() {
        return password;
    }


    @JsonProperty("database")
    public DataSourceFactory getDataSourceFactory() {
        return database;
    }
    
    public void setDatabase(DataSourceFactory database) {
        this.database = database;
    }
}
