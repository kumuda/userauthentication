package com.userAuthentication;

import io.dropwizard.Application;
import io.dropwizard.auth.AuthFactory;
import io.dropwizard.auth.basic.BasicAuthFactory;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.jdbi.DBIFactory;
import io.dropwizard.jdbi.bundles.DBIExceptionsBundle;
import io.dropwizard.migrations.MigrationsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

import org.skife.jdbi.v2.DBI;

import com.fasterxml.jackson.datatype.guava.GuavaModule;
import com.userAuthentication.auth.UserAuthenticator;
import com.userAuthentication.db.UserDao;
import com.userAuthentication.model.User;
import com.userAuthentication.resources.UserResource;

public class UserAuthenticationApplication extends
		Application<UserAuthenticationConfiguration> {

	public static void main(final String[] args) throws Exception {
		new UserAuthenticationApplication().run(args);
	}

	@Override
	public String getName() {
		return "UserAuthenticator";
	}

	@Override
	public void initialize(Bootstrap<UserAuthenticationConfiguration> bootstrap) {
		bootstrap.addBundle(new DBIExceptionsBundle());
		bootstrap
				.addBundle(new MigrationsBundle<UserAuthenticationConfiguration>() {
					@Override
					public DataSourceFactory getDataSourceFactory(
							UserAuthenticationConfiguration configuration) {
						return configuration.getDataSourceFactory();
					}
				});

		bootstrap.getObjectMapper().registerModule(new GuavaModule());

	}

	@Override
	public void run(final UserAuthenticationConfiguration configuration,
			final Environment environment) {

		final DBIFactory factory = new DBIFactory();
		final DBI jdbi = factory.build(environment,
				configuration.getDataSourceFactory(), "mysql");
		final UserDao userDao = jdbi.onDemand(UserDao.class);

		environment.jersey().register(
				AuthFactory.binder(new BasicAuthFactory<>(
						new UserAuthenticator(configuration.getUserName(),
								configuration.getPassword()), "SECURITY REALM",
						User.class)));
		// Resources are registered here
		environment.jersey().register(new UserResource(userDao));
	}

}
