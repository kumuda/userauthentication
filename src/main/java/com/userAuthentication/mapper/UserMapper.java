package com.userAuthentication.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import com.userAuthentication.model.User;

public class UserMapper  implements ResultSetMapper<User> {

	@Override
	public User map(int index, ResultSet resultSet, StatementContext context)
			throws SQLException {
		User user = new User();
		System.out.println("Result set id----->" + resultSet.getInt(1));
		user.setId(resultSet.getInt(1));
        user.setUserName(resultSet.getString("username"));
        user.setPassword(resultSet.getString("password"));
        return user;
	}

}
