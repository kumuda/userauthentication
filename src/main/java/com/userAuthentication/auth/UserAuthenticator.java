package com.userAuthentication.auth;

import com.google.common.base.Optional;
import com.userAuthentication.model.User;

import io.dropwizard.auth.AuthenticationException;
import io.dropwizard.auth.Authenticator;
import io.dropwizard.auth.basic.BasicCredentials;

public class UserAuthenticator implements Authenticator<BasicCredentials, User> {

	private String userName;

	private String password;

	public UserAuthenticator(String userName, String password) {
		this.userName = userName;
		this.password = password;
	}

	public Optional<User> authenticate(BasicCredentials credentials)
			throws AuthenticationException {
		if (password.equals(credentials.getPassword())
				&& userName.equals(credentials.getUsername())) {
			return Optional.of(new User());
		} else {
			return Optional.absent();
		}
	}

	 /*@Override
	    public Optional<User> authenticate(BasicCredentials credentials)
	            throws AuthenticationException {
	        if ("crimson".equals(credentials.getPassword())) {
	            return Optional.of(new User());
	        } else {
	            return Optional.absent();
	        }
	    }*/

}
